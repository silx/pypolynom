# coding: utf-8

"""This is a simple demonstration library"""

__authors__ = ["Pierre Knobel", "Jerome Kieffer", "Pierre Palero",
               "Henri Payno", "Armando Sole", "Valentin Valls",
               "Thomas Vincent"]
__date__ = "23/10/2021"
__license__ = "KIT"


from . import mathutil
import logging


def polynom(a, b, c):
    """Solve the polygon of order two.

    .. math:: a\cdotx^2 + b\cdotx + c = 0

    :param float a: a value of the polynom
    :param float b: b value of the polynom
    :param float c: c value of the polynom
    :rtype: List[float]
    """
    if a == 0:
        # Not a polynom
        raise ValueError("Not a quadratic equation (a==0)")
    delta = mathutil.pow2(b) - 4.0 * a * c
    solutions = []
    if delta > 0:
        solutions.append((-b + mathutil.sqrt(delta)) / (2.0 * a))
        solutions.append((-b - mathutil.sqrt(delta)) / (2.0 * a))
    elif delta == 0:
        solutions.append(-b/(2.0*a))
    else:
        # Delta is negative, no real solution
        logging.warning("No real solution for %s x^2 + %s x + %s" % (a, b, c))
    return solutions
